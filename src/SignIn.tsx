import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import logo from './img/logo.png'; // Tell Webpack this JS file uses this image
import { Card, CardContent } from '@mui/material';
import './signin.css';


function Copyright(props: any) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://mui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

export default function SignIn() {
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        // eslint-disable-next-line no-console
        console.log({
            email: data.get('email'),
            password: data.get('password'),
        });
    };

    return (
        <ThemeProvider theme={theme}>
            <Grid container
                spacing={0}
                direction="column"
                alignItems="center"
            >
                <Grid item>
                    <Card sx={{
                        marginTop: {
                            sx: 5, // 100%
                            sm: 5,
                            md: 5,
                        },
                        minWidth: 275, width: {
                            sx: 1.0, // 100%
                            sm: 250,
                            md: 350,
                        }
                    }}>
                        <CardContent>
                            <Container component="main" maxWidth="xs">
                                <CssBaseline />
                                <Box
                                    sx={{
                                        marginTop: 4,
                                        display: 'flex',
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                    }}
                                >
                                    <Grid container>
                                        <Grid item xs>
                                            <Typography component="h1" variant="h5">
                                                KYPASS
                                            </Typography>
                                            <Typography variant="caption" display="block" gutterBottom>
                                                Keep your password secret
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Avatar src={logo} alt="Logo Telkom" sx={{ width: 80, height: 50 }} variant="square">
                                            </Avatar>
                                        </Grid>
                                    </Grid>
                                    <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                                        <TextField
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            label="NIK"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                        />
                                        <Grid container>
                                            <Grid item xs>

                                            </Grid>
                                            <Grid item>
                                                <Link href="#" variant="body2">
                                                    Forgot password?
                                                </Link>
                                            </Grid>
                                        </Grid>
                                        <TextField
                                            margin="normal"
                                            required
                                            fullWidth
                                            name="password"
                                            label="Password"
                                            type="password"
                                            id="password"
                                            autoComplete="current-password"
                                        />

                                        <Button
                                            type="submit"
                                            fullWidth
                                            variant="contained"
                                            sx={{ mt: 3, mb: 2, pt: 1, pb: 1 }}
                                        >
                                            Sign In
                                        </Button>
                                        <Grid container spacing={0}
                                        sx={{ pb: 3 }}
                                            direction="column"
                                            alignItems="center">

                                            <Grid item>
                                                <Link href="#" variant="body2">
                                                    {"Don't have an account? Sign Up"}
                                                </Link>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </Box>
                            </Container>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
}